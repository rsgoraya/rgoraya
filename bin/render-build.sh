#!/usr/bin/env bash
# exit on error
set -o errexit

bundle _1.17.3_  install
bundle exec rails assets:precompile
bundle exec rails assets:clean