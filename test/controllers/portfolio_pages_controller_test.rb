require 'test_helper'

class PortfolioPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get portfolio" do
    get :portfolio
    assert_response :success
  end

  test "should get projects" do
    get :projects
    assert_response :success
  end

  test "should get resume" do
    get :resume
    assert_response :success
  end

end
