class CreateUserContacts < ActiveRecord::Migration
  def change
    create_table :user_contacts do |t|
      t.string :name
      t.string :job
      t.string :title
      t.string :address
      t.string :phone_number
      t.string :email
      t.string :picture

      t.timestamps null: false
    end
  end
end
