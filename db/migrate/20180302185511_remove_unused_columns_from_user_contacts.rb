class RemoveUnusedColumnsFromUserContacts < ActiveRecord::Migration
  def change
    remove_column :user_contacts, :title, :string
    remove_column :user_contacts, :picture, :string
  end
end
