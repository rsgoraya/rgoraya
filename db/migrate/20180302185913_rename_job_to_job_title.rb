class RenameJobToJobTitle < ActiveRecord::Migration
  def change
    rename_column :user_contacts, :job, :job_title
  end
end
