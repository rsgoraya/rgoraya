json.array!(@user_contacts) do |user_contact|
  json.extract! user_contact, :id, :name, :job, :title, :address, :phone_number, :email, :picture
  json.url user_contact_url(user_contact, format: :json)
end
