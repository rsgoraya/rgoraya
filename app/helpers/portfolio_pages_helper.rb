module PortfolioPagesHelper

  def project_type_logo(repository_type)
    if repository_type == "Github"
      "portfolio/github_logo.png"
    elsif repository_type == "BitBucket"
      "portfolio/bitbucket_logo.png"
    end
  end

end
