module ApplicationHelper

  def body_class_name
    "#{controller_name}_#{action_name}"
  end

  def page_title
    "Raminder Goraya#{@page_title ? " | " + @page_title : ""}"
  end

  def nav_link(link_text, link_path)
    class_name = current_page?(link_path) ? 'active' : ''

    content_tag(:li, :class => class_name) do
      link_to link_text, link_path
    end
  end

  def abbr(abbr, full_form)
    content_tag(:abbr, :title => full_form) do
      abbr
    end
  end

  def meta_description
    "Raminder Goraya's portfolio. An experienced software engineer, passionate about developing web and mobile apps offering creative and responsive user interfaces, powered by robust and efficient back-end."
  end

end
