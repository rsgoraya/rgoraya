if (!window.R) { window.R = {}; }

R.backToTop = (function($, _) {

    var back_to_top = function() {
        this.bind_events();
    };

    back_to_top.prototype = {

        bind_events : function () {
            $('#back_to_top').on('click', _.bind(this._handle_back_to_top, this));
        },

        _handle_back_to_top : function (e) {
            e.preventDefault();
            $('html,body').animate({scrollTop:0},'300');
            return false;
        }

    };

    return back_to_top;

})(window.jQuery, window._);

R.scrollSpy = (function($, _) {

    var scroll_spy = function($target, offset) {
        this._$target = $target;
        this._offset = offset;
        this.set_scroll_spy();
    };

    scroll_spy.prototype = {

        set_scroll_spy : function () {
            $("body").scrollspy({
                target: this._$target,
                offset: this._offset
            });
        }

    };

    return scroll_spy;

})(window.jQuery, window._);