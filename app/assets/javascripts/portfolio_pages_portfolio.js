if (!window.R) { window.R = {}; }

R.portfolio = (function($) {

    var portfolio_page = {
            init : function () {
                this._bind_events();
                this.back_to_top = new R.backToTop();
                this.scroll_spy = new R.scrollSpy("#portfolioScrollSpy", 20);
            },

            _compile_hogan : function () {
                return Hogan.compile($('#enlargePreviewImgTemplate').html());
            },

            _bind_events : function () {
                var template = this._compile_hogan();
                $('a.thumbnail').on('click', function (e) {
                    var $target = $(this),
                        context = {
                            path:    $target.data('imgpath'),
                            height:  $target.data('height'),
                            width:   $target.data('width'),
                            crop:    $target.data('crop')
                        };
                    $('#enlargePreviewModal .modal-body').html(template.render(context));
                    $("#enlargePreviewModal").modal();
                });
            }
        },


        portfolio = function() {
            this._page = portfolio_page;
            this._page.init();
        };

    return portfolio;

})(window.jQuery);


$(document).ready(function () { var page = new R.portfolio(); });
