if (!window.R) { window.R = {}; }

R.resume = (function($, _) {

    var resume = function() {
            this.scroll_spy = new R.scrollSpy("#resumeScrollSpy", 20);
            this.back_to_top = new R.backToTop();
        };

    return resume;

})(window.jQuery, window._);


$(document).ready(function () { var page = new R.resume(); });
