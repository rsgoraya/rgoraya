if (!window.R) { window.R = {}; }

R.contactPage = (function($) {

    var contact_page = function() {
        this.bind_contact_form_submit_event();
        this.back_to_top = new R.backToTop();
    };

    contact_page.prototype = {

        bind_contact_form_submit_event : function() {
            var that = this;
            $("form#new_contact").on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                that._remove_form_errors();
                that._hide_message();

                var data = $(this).serialize();

                $.post('/contacts', data, function(json) {
                    $("form#new_contact")[0].reset();
                    that._show_message("Dear " + json.name + " your message was submitted successfully", "alert-success");
                }).error(function (json) {
                    that._display_form_errors(json.responseJSON);
                    that._show_message("Your message could not be submitted at this time", "alert-danger");
                });
            })
        },

        _show_message : function (message, klass) {
            $("#message")
                .find('.text').html(message)
                .end()
                .addClass(klass)
                .slideDown(400);
            $("html, body").animate({ scrollTop: 0 }, "300");
            return false;
        },

        _hide_message : function () {
            $("#message")
                .find('.text').html('')
                .end()
                .removeClass('alert-success alert-danger')
                .slideUp(100);
        },

        _display_form_errors : function (errors) {
            $.each(errors, function(key, error) {
                $("form#new_contact")
                    .find('.contact_' + key).addClass('has-error')
                    .append("<span class='help-block'>" + error + "</span>")
            });
        },

        _remove_form_errors : function () {
            $("form#new_contact")
                .find('.form-group')
                .removeClass('has-error')
                .find('.help-block').remove();
        }
    };

    return contact_page;

})(window.jQuery);


$(document).ready(function() {
    var contact_page = new R.contactPage();
});