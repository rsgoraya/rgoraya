class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    #@contact.request = request

    respond_to do |format|
      if @contact.deliver
        format.json { render json: @contact, status: :created }
      else
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end

  end
end
