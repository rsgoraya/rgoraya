class Api::V1::UserContactsController < Api::V1::BaseController

  before_action :add_cors_to_json

  def index
    if params[:search]
      respond_with UserContact.search(params[:search]).order(params[:order])
    else
      respond_with UserContact.all.order(params[:order])
    end
  end

  def show
  end

  # GET /user_contacts/new
  def new
    @user_contact = UserContact.new
  end

  # GET /user_contacts/1/edit
  def edit
  end

  def create
    respond_with :api, :v1, UserContact.create(user_contact_params)
  end

  def update
    user_contact = UserContact.find(params["id"])
    user_contact.update_attributes(user_contact_params)
    respond_with user_contact, json: user_contact
  end

  def destroy
    respond_with UserContact.destroy(params[:id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_contact
      @user_contact = UserContact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_contact_params
      params.require(:user_contact).permit(:name, :job_title, :address, :phone_number, :email)
    end

    def add_cors_to_json
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
      headers['Access-Control-Request-Method'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    end
end
