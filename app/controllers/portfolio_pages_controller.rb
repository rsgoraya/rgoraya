class PortfolioPagesController < ApplicationController
  def home
  end

  def portfolio
    @page_title = "Portfolio"
  end

  def projects
    @page_title = "Projects"
    # github       = Github.new basic_auth: "#{Rails.configuration.github[:username]}:#{Rails.configuration.github[:password]}"
    github       = Github.new do |config|
      config.basic_auth         = "#{Rails.configuration.github[:username]}:#{Rails.configuration.github[:password]}"
      config.connection_options = {headers: {"X-GitHub-OTP" => '2fa token'}}
    end
    @git_repos   = github.repos.list user: 'rgoraya'
    @git_profile = github.users.get  user: 'rgoraya'

    # bitbucket   = BitBucket.new login: Rails.configuration.bitbucket[:username], password: Rails.configuration.bitbucket[:password]
    # @bit_repos   = bitbucket.repos.list
    # @bit_profile = bitbucket.user_api.profile.user

    # @repo_types = [ { :repo_type => "Github", :repos => @git_repos.to_a }, { :repo_type => "BitBucket", :repos => @bit_repos } ]

    @repo_types = [ { :repo_type => "Github", :repos => @git_repos.to_a } ]

  end

  def resume
    @page_title = "Resume"
  end

  def resume_pdf
    request.format = "pdf" unless params[:format]
    @page_title = "Resume PDF"
    respond_to do |format|
      format.pdf do
        render :pdf    => "raminder_goraya",
               :disposition => "inline",
               :layout      => "pdf_document.html.haml"
      end

      format.html
    end
  end

end
