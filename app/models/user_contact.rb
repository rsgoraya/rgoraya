class UserContact < ActiveRecord::Base
  def self.search(search)
    where("lower(name) LIKE ?", "%#{search.downcase}%")
  end
end
