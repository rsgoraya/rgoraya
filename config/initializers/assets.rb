# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

%w( portfolio_pages_resume portfolio_pages_portfolio portfolio_pages_projects welcome_index contacts_new user_contacts_index user_contacts_show demo_index ).each do |controller_action|
  Rails.application.config.assets.precompile += ["#{controller_action}.js"]
end

%w( portfolio_pages welcome contacts pdf_document user_contacts demo).each do |controller|
  Rails.application.config.assets.precompile += ["#{controller}.css"]
end