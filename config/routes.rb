Rails.application.routes.draw do

  root 'welcome#index'

  get 'portfolio_pages/home'

  get '/portfolio' => 'portfolio_pages#portfolio', as: :portfolio
  get '/projects'  => 'portfolio_pages#projects',  as: :projects
  get '/resume'    => 'portfolio_pages#resume',    as: :resume
  get '/resume-pdf'=> 'portfolio_pages#resume_pdf',as: "resume-pdf"

  get '/contact'  => 'contacts#new',               as: :contact
  resources :contacts, only: [:new, :create]

  namespace :api do
    namespace :v1 do
      resources :user_contacts, only: [:index, :create, :destroy, :update]
    end
  end

  resources :demo, only: [:index]
  
  #get 'portfolio_pages/portfolio'
  #
  #get 'portfolio_pages/projects'
  #
  #get 'portfolio_pages/contact'
  #
  #get 'portfolio_pages/resume'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
